import react from '@vitejs/plugin-react'
import { readdirSync } from 'fs'
import { defineConfig } from 'vite'
import checker from 'vite-plugin-checker'

const libs = `${__dirname}/src/lib/`
const proxy = { target: 'https://localhost:8443', changeOrigin:true, secure: false, headers: { "Connection": "keep-alive" } }
const wsproxy = { target: proxy.target, ws: true, changeOrigin:true, secure: false }

const autochunked = ['date-fns', 'react-quill', 'quill', 'xlsx', 'react-color']

export default defineConfig({

  base: "/ac/",

  server: {

    host: true,
    
    port: 3000,

    proxy: {
      '/ac/config.json': proxy,
      '/domain': proxy,
      '/admin': proxy,
      '/testmail': proxy,
      '/oauth2': proxy,
      '/export': proxy,
      '/event': wsproxy
    },

  },

  resolve: {
    preserveSymlinks: true,
    alias: [
      ...readdirSync(libs).map(name => ({ find: name, replacement: `${libs}/${name}` })),
      { find: /^#(.*)$/, replacement: `${__dirname}/src/$1` },

    ]
  }

  ,

  build: {

    sourcemap: true,
    rollupOptions: {

      output: {

        manualChunks: id => {

          if (autochunked.some(c => id.includes(c)))
            return

          if (id.includes('nats.ws'))
            return 'natsws'

          if (id.includes('node_modules'))
            return 'vendor'

        }

      }
    }
  },

  plugins: [

    react(),

    checker({
      overlay: {
        initialIsOpen: false,
        position: 'br'
      },
      typescript: true,
      eslint: {
        lintCommand: 'eslint "src/**/*{.ts,.tsx}"'
      }
    })
  ]
})
