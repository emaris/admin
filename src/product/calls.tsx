import { useCall } from "apprise-frontend-core/client/call"
import { Product } from "./model"
import { productApi } from "./constants"

export const useProductCalls = () => {

    const call = useCall()

    const self = {

        fetchAll: () => call.at(`${productApi}/raw`).get<Product[]>()

    }

    return self

}