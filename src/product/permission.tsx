import { any } from "apprise-frontend-core/authz/constants"
import { useL } from "apprise-frontend-core/intl/multilang"
import { useTenancyOracle } from "apprise-frontend-iam/authz/tenant"
import { PermissionBox, PermissionBoxProps } from "apprise-frontend-iam/permission/permissionbox"
import { UserPermissionModule } from "apprise-frontend-iam/user/modules"
import { Label } from "apprise-ui/label/label"
import { ProductIcon, productIcon, productPlural, productSingular, productSubmissionPlural, productSubmissionSingular, productSubmissionType, productType } from "./constants"
import { Product } from "./model"
import { useProductStore } from "./store"

export const baseProductAction = { icon:<ProductIcon />, type: productType, resource :any }

export const productAdminActions = {
    manage:  {...baseProductAction, labels:["manage"], shortName:"actions.product_manage_short",name:"actions.product_manage_name", description: "actions.product_manage_desc"}
    ,
    edit:  {...baseProductAction, labels:["manage","edit"], shortName:"actions.product_edit_short",name:"actions.product_edit_name", description: "actions.product_edit_desc"}
    ,
    assess:  {...baseProductAction, labels:["manage","assess"], shortName:"actions.product_assess_short",name:"actions.product_assess_name", description: "actions.product_assess_desc"}
}

export const productTenantActions = {
    manage:  {...baseProductAction, labels:["manage"], shortName:"actions.product_manage_short",name:"actions.product_manage_name", description: "actions.product_manage_desc"}
}

export const productActions = productAdminActions

export const useProductPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: productIcon,
        type: productType,
        plural: productPlural,

        box: props => oracle.given(props.edited).hasNoTenant() && <ProductPermissionBox {...props} />

    } as UserPermissionModule<Product>

}


export const ProductPermissionBox = (props: Partial<PermissionBoxProps<Product>>) => {

    const { ...rest } = props

    const l = useL()

    const oracle = useTenancyOracle()

    const store = useProductStore()

    const target = props.userRange?.[0]

    const actions = oracle.given(target).hasNoTenant() ? productAdminActions : productTenantActions

    return <PermissionBox<Product>

        resourceSingular={productSingular}

        permissionRange={Object.values(actions)}
        maxPrivilege={actions.manage ?? {}}

        resourcePlural={productPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<ProductIcon />} title={l(s.name)} />}
        resourceRange={store.all()}

        
        wildcard
        preselectWildcard


        {...rest} />
}


const baseProductSubmissiontAction = { icon:<ProductIcon />, type: productSubmissionType, resource :any }

export const productSubmissionActions = {  
    
    manage:  {...baseProductSubmissiontAction, labels:["manage"], shortName:"actions.product_submission_manage_short",name:"actions.product_submission_manage_name", description: "actions.product_submission_manage_desc"}
    ,
    edit:  {...baseProductSubmissiontAction, labels:["manage","edit"], shortName:"actions.product_submission_edit_short",name:"actions.product_submission_edit_name", description: "actions.product_submission_edit_desc"}
    ,
    submit:  {...baseProductSubmissiontAction, labels:["manage","submit"], shortName:"actions.product_submission_submit_short",name:"actions.product_submission_submit_name", description: "actions.product_submission_submit_desc"}
    ,
    message:  {...baseProductSubmissiontAction, labels:["manage","message"], shortName:"actions.product_submission_message_short",name:"actions.product_submission_message_name", description: "actions.product_submission_message_desc"}
   
}


export const useProductSubmissionPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: ProductIcon,
        type: productType,
        plural: productSubmissionPlural,

        box: props => oracle.given(props.edited).isTenantUser() && <ProductSubmissionPermissionBox {...props} />

    } as UserPermissionModule<Product>

}

export const ProductSubmissionPermissionBox = (props: Partial<PermissionBoxProps<Product>>) => {

    const { ...rest } = props

    const l = useL()

    const store = useProductStore()

  
    return <PermissionBox<Product>

        resourceSingular={productSubmissionSingular}

        permissionRange={Object.values(productSubmissionActions)}
        maxPrivilege={productSubmissionActions.manage ?? {}}

        resourcePlural={productSubmissionPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<ProductIcon />} title={l(s.name)} />}
        resourceRange={store.all()}

        tenantResource
        
        wildcard
        preselectWildcard


        {...rest} />
}