import { State } from "apprise-frontend-core/state/api";
import { StateProvider } from "apprise-frontend-core/state/provider";
import { useRenderGuard } from "apprise-frontend-core/utils/renderguard";
import { PropsWithChildren, createContext } from "react";
import { Product } from "./model";
import { useProductStore } from "./store";

export type ProductCache = {
    all: Product[]
}

export const initialCacheState: ProductCache = {
    all: []
}

export const ProductCacheContext = createContext<State<ProductCache>>(undefined!)

const ProductProvider = (props: PropsWithChildren) => {

    return <StateProvider initialState={initialCacheState} context={ProductCacheContext} >
        {props.children}
    </StateProvider>


}

const ProductLoader = (props: PropsWithChildren) => {

    const { children } = props

    const store = useProductStore()

    const activate = () => store.fetchAll()

    const { content } = useRenderGuard({

        render: children,
        orRun: activate
        
    })

    return content

}

export const ProductsLoader = (props: PropsWithChildren) => {

    const { children } = props

    return <ProductProvider>
        <ProductLoader>
            {children}
        </ProductLoader>
    </ProductProvider>

}