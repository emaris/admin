import { useCrud } from "apprise-ui/utils/crudtask"
import { useContext } from "react"
import { ProductCacheContext } from "./provider"
import { useProductCalls } from "./calls"
import { productApi, productPlural, productSingular } from "./constants"
import { usePreload } from 'apprise-frontend-core/client/preload'
import { Product } from './model'


export const useProductStore = () => {


    const state = useContext(ProductCacheContext)

    const crud = useCrud({ singular: productSingular, plural: productPlural })

    const calls = useProductCalls()

    const preload = usePreload()

    const self = {

        all: () => state.get().all

        ,

        fetchAll: crud.fetchAllWith(async () => {

            const products = await ( preload.get<Product[]>(productApi) ?? calls.fetchAll())

            state.set(s => {

                s.all = products

            })

            return products

        }).done()

    }

    return self

}