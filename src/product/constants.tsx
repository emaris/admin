import { IoMdToday } from 'react-icons/io'

export const productApi = '/product'

export const productSingular = "product.singular"
export const productPlural = "product.plural"

export const productType="product"
export const productSubmissionType = "product-submission"
export const productIcon= IoMdToday
export const ProductIcon = IoMdToday
export const productRoute="/product" 

export const productSubmissionSingular = "submission.product_singular"
export const productSubmissionPlural = "submission.product_plural"


export const prodImportanceCategory = "TGC-product-importancescale"