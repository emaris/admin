import { useCall } from "apprise-frontend-core/client/call"
import { Campaign, CampaignDto, useCampaignModel } from "./model"
import { campaignApi } from "./constants"


export const useCampaignCalls = () => {

    const call = useCall()

    const { intern } = useCampaignModel()

    const self = {

        fetchAll: () => call.at(campaignApi).get<CampaignDto[]>().then(campaigns => campaigns.map(intern) as Campaign[])

    }

    return self

}