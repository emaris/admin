import { any } from "apprise-frontend-core/authz/constants"
import { useL } from "apprise-frontend-core/intl/multilang"
import { useTenancyOracle } from "apprise-frontend-iam/authz/tenant"
import { PermissionBox } from "apprise-frontend-iam/permission/permissionbox"
import { UserPermissionBoxProps, UserPermissionModule } from "apprise-frontend-iam/user/modules"
import { Label } from "apprise-ui/label/label"
import { CampaignIcon, campaignIcon, campaignPlural, campaignSingular, campaignType } from "./constants"
import { Campaign } from "./model"
import { useCampaignStore } from "./store"

export const baseCampaignAction = { icon: <CampaignIcon />, type: campaignType, resource: any }

export const campaignAdminActions = {
    manage: { ...baseCampaignAction, labels: ["manage"], shortName: "actions.campaign_manage_short", name: "actions.campaign_manage_name", description: "actions.campaign_manage_desc" }
}

export const campaignActions = campaignAdminActions

export const useCampaignPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: campaignIcon,
        type: campaignType,
        plural: campaignPlural,

        box: props => oracle.given(props.edited).hasNoTenant() && <CampaignPermissionBox {...props} />

    } as UserPermissionModule<Campaign>

}


export const CampaignPermissionBox = (props: Partial<UserPermissionBoxProps<Campaign>>) => {

    const { edited, ...rest } = props

    const l = useL()

    const store = useCampaignStore()

    const actions = campaignActions

    return <PermissionBox<Campaign>

        resourceSingular={campaignSingular}

        permissionRange={Object.values(actions)}
        maxPrivilege={actions.manage ?? {}}

        resourcePlural={campaignPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<CampaignIcon />} title={l(s.name)} />}
        resourceRange={store.all()}


        wildcard
        preselectWildcard


        {...rest} />
}