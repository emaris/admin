import { eventType } from "#event/constants"
import { AssetType } from "#model/model"
import { productPlural, productType } from "#product/constants"
import { requirementPlural, requirementType } from "#requirement/constants"
import { tenantPlural, tenantType } from "apprise-frontend-iam/tenant/constants"

export type Campaign = AssetType & {}

export type CampaignDto = {
    campaign: Campaign
    summary: any
}

export const useCampaignModel = () => {


    const self = {

        intern: (c: CampaignDto):Campaign => c.campaign
    }

    return self

}

export type DateLocation = "home" | "local" | "universal" | "original"


export const dateLocationOptions : { [key in DateLocation] : string } = {

    local: "campaign.default_location_local",
    home : "campaign.default_location_home",
    original: "campaign.default_location_original",
    universal: "campaign.default_location_none",
}

export const approveCycleOptions = {
    none: "campaign.approve_cycle_none",
    all: "campaign.approve_cycle_all",
    [requirementType]: "campaign.approve_cycle_requirement",
    [productType]: "campaign.approve_cycle_product"
}

export type DashboardView = 'summary' | typeof tenantType | typeof requirementType | typeof productType | typeof eventType

export const dashboardOptions : { [key in DashboardView] : string } = {
    'summary':"dashboard.summary_name",
    [tenantType]:tenantPlural,
    [requirementType]:requirementPlural,
    [productType]:productPlural,
    [eventType]:"dashboard.calendar_name"
}

export type CalendarView = 'month' | 'list'

export const calendarOptions : { [key in CalendarView] : string } = {
    'list' : "dashboard.calendar_list_view",
    'month' : "dashboard.calendar_monthly_view"
}

export type RelativeDate = {

    kind: 'relative'
    period: {
        number: number,
        unit: 'months' | 'days' | 'weeks',
        direction: 'before' | 'after'
    }
    target: string

}

export const defaultRelativeDate = {kind: 'relative', period: {number: 1, unit: 'months', direction: 'before'}} as RelativeDate

export type ApproveCycle = typeof requirementType | typeof productType | 'all' | 'none'

export type CampaignSettings = {

    complianceScale?: string
    timelinessScale?: string
    approveCycle: ApproveCycle
    liveGuard?: boolean
    statisticalHorizon: number
    partyCanSeeNotApplicable: boolean
    defaultRelativeDate: RelativeDate
    timeZone: string
    suspendOnEnd: boolean
    suspendSubmissions: boolean
}
