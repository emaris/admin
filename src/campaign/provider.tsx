import { State } from "apprise-frontend-core/state/api";
import { StateProvider } from "apprise-frontend-core/state/provider";
import { useRenderGuard } from "apprise-frontend-core/utils/renderguard";
import { PropsWithChildren, createContext } from "react";
import { useCampaignStore } from "./store";
import { Campaign } from "./model";

export type CampaignCache = {
    all: Campaign[]
}

export const initialCacheState: CampaignCache = {
    all: []
}

export const CampaignCacheContext = createContext<State<CampaignCache>>(undefined!)

const CampaignProvider = (props: PropsWithChildren) => {

    return <StateProvider initialState={initialCacheState} context={CampaignCacheContext} >
        {props.children}
    </StateProvider>


}

const CampaignLoader = (props: PropsWithChildren) => {

    const { children } = props

    const store = useCampaignStore()

    const activate = () => store.fetchAll()


    const { content } = useRenderGuard({

        render: children,
        orRun: activate
        
    })

    return content

}

export const CampaignsLoader = (props: PropsWithChildren) => {

    const { children } = props

    return <CampaignProvider>
        <CampaignLoader>
            {children}
        </CampaignLoader>
    </CampaignProvider>

}