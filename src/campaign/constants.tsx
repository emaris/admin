

import { AiOutlineCheckSquare, AiOutlineClockCircle } from "react-icons/ai"
import { RiCheckboxMultipleBlankLine } from 'react-icons/ri'
import { FiLayers } from "react-icons/fi"
import { MdSpaceDashboard } from "react-icons/md"

export const campaignApi = '/campaign'

export const campaignSingular = "campaign.singular"
export const campaignPlural = "campaign.plural"

export const complianceType="compliance"
export const complianceSingular = "campaign.compliance_singular"
export const compliancePlural = "campaign.compliance_plural"
export const complianceIcon = AiOutlineCheckSquare

export const timelinessType="timeliness"
export const timelinessSingular = "campaign.timeliness_singular"
export const timelinessPlural = "campaign.timeliness_plural"
export const timelinessIcon = AiOutlineClockCircle

export const campaignType ="campaign"
export const campaignIcon = FiLayers
export const CampaignIcon = FiLayers
export const campaignRoute ="/campaign"

export const dashboardIcon = MdSpaceDashboard
export const DashboardIcon = MdSpaceDashboard

export const submissionType="submission"
export const submissionSingular = "campaign.submission_singular"
export const submissionPlural = "campaign.submission_plural"
export const submissionIcon = RiCheckboxMultipleBlankLine