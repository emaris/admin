import { Select } from 'antd';
import { useT } from "apprise-frontend-core/intl/language";
import { SettingRenderProps } from "apprise-frontend-streams/settings/model";
import { CategoryBox } from "apprise-frontend-tags/category/categorybox";
import { useCategoryStore } from "apprise-frontend-tags/category/store";
import { ChoiceBox } from "apprise-ui/choicebox/choicebox";
import { Sized, Tall, Wide } from "apprise-ui/component/model";
import { Field, useFieldProps } from "apprise-ui/field/field";
import { Fielded, Uncontrolled } from "apprise-ui/field/model";
import { Form } from "apprise-ui/form/form";
import { NumberBox } from "apprise-ui/numberbox/numberbox";
import { SliderBox } from "apprise-ui/sliderbox/slider";
import { SwitchBox } from "apprise-ui/switchbox/switchbox";
import { ZonePickerBox } from "apprise-ui/timezone/zonepicker";
import { addDays, differenceInDays } from 'date-fns';
import { AiOutlineDown } from 'react-icons/ai';
import { complianceType, timelinessType } from "./constants";
import { ApproveCycle, CampaignSettings, RelativeDate, approveCycleOptions, defaultRelativeDate } from "./model";

import './styles.scss';

const horizonSteps = 5

export const CampaignSettingsForm = (props: SettingRenderProps<CampaignSettings>) => {
    
    const t = useT()

    const { settings, onChange } = props

    const categories = useCategoryStore()

    const currentComplianceScale = categories.safeLookupCategory(settings.complianceScale)
    const currentTimelinessScale = categories.safeLookupCategory(settings.timelinessScale)

    const now = new Date()

    const maxHorizonDays = differenceInDays(addDays(now, 549), now)

    const computedMaxHorizonDays = maxHorizonDays - (maxHorizonDays % horizonSteps)

    return <Form>


        <CategoryBox
            label={t('campaign.compliance_scale_lbl')}
            msg={t('campaign.compliance_scale_msg')}
            help={t('campaign.compliance_scale_help')}
            type={complianceType}
            onChange={t => onChange({...settings, complianceScale: t?.id})}
        >
            {currentComplianceScale}
        </CategoryBox>

        <CategoryBox
            label={t('campaign.timeliness_lbl')}
            msg={t('campaign.timeliness_msg')}
            help={t('campaign.timeliness_help')}
            type={timelinessType}
            onChange={t => onChange({...settings, timelinessScale: t?.id})}
        >
            {currentTimelinessScale}
        </CategoryBox>

        <ChoiceBox<ApproveCycle>
            label={t('campaign.approve_cycle_lbl')}
            msg={t('campaign.approve_cycle_msg')}
            help={<>{t('campaign.approve_cycle_help1')}<br/>{t('campaign.approve_cycle_help2')}</>}
            radio 
            value={settings.approveCycle}
            defaultValue='none'
            onChange={v => onChange({...settings, approveCycle: v!})}
        >
            {Object.keys(approveCycleOptions).map((op, i) => <ChoiceBox.Option key={i} title={t(approveCycleOptions[op])} value={op} />)}
        </ChoiceBox>

        <SliderBox
            label={t('campaign.statistical_horizon_lbl')}
            msg={t('campaign.statistical_horizon_msg')}
            help={t('campaign.statistical_horizon_help')}
            step={horizonSteps}
            min={0}
            max={computedMaxHorizonDays}
            onChange={v => onChange({...settings, statisticalHorizon: v!})}
            tipRender={val => val === computedMaxHorizonDays ? t('campaign.end_of_campaign') : `${val}`}
            
        >
            {settings.statisticalHorizon ?? 100}
        </SliderBox>

        <SwitchBox 
            label={t('campaign.suspend_on_end_lbl')}
            msg={t('campaign.suspend_on_end_msg')}
            help={t('campaign.suspend_on_end_help')}
            onChange={v => onChange({...settings, suspendOnEnd: !!v})}
        >
            {settings.suspendOnEnd}
        </SwitchBox>

        <SwitchBox 
            label={t('campaign.liveguard_lbl')}
            msg={t('campaign.liveguard_msg')}
            help={t('campaign.liveguard_help')}
            onChange={v => onChange({...settings, liveGuard: !!v})}
        >
            {settings.liveGuard}
        </SwitchBox>

        <ZonePickerBox
            label={t('campaign.timezone_lbl')}
            msg={t('campaign.timezone_msg')}
            help={t('campaign.timezone_help')}
            onChange={v => onChange({...settings, timeZone: v?.option!})}
        >
            {settings.timeZone}
        </ZonePickerBox>

        <PureRelativeDateBox
            label={t('campaign.default_relativedate_lbl')}
            onChange={v => onChange({...settings, defaultRelativeDate: v!})}
        >
            {settings.defaultRelativeDate ?? defaultRelativeDate}
        </PureRelativeDateBox>

    </Form>

}


export type PureRelativeDateBoxProps<T> = Fielded<T> & Sized & Wide & Tall & Uncontrolled & {children: RelativeDate}

const units = {

    "days": { max: 999 },
    "weeks": { max: 999 },
    "months": { max: 999 }
}

const PureRelativeDateBox = (clientprops: Partial<PureRelativeDateBoxProps<RelativeDate>>) => {
    const t = useT()

    const props = useFieldProps(clientprops)

    const { children:relativeDate=defaultRelativeDate, onChange=()=>null, ...rest } = props

    const changePeriod = (num: number | undefined) => onChange({ ...relativeDate, period: { ...relativeDate.period, number: num! } })
    const changeUnit = (unit: "months" | "days" | "weeks") => onChange({ ...relativeDate, period: { ...relativeDate.period, unit } })
    const changeDirection = (direction: any) => onChange({ ...relativeDate, period: { ...relativeDate.period, direction } })
    
    const contents = <><div className="databox-period">
        <NumberBox className="datebox-number" min={0} max={units[relativeDate.period.unit].max} onChange={changePeriod}>{relativeDate.period.number}</NumberBox>
        <Select className="datebox-unit" popupClassName="date-option" suffixIcon={<AiOutlineDown />} defaultValue={relativeDate.period.unit} onChange={changeUnit} >
            <Select.Option className="datebox-option" value="days">{t("campaign.date_days")}</Select.Option>
            <Select.Option className="datebox-option" value="months">{t("campaign.date_months")}</Select.Option>
            <Select.Option className="datebox-option" value="weeks">{t("campaign.date_weeks")}</Select.Option>
        </Select>
        <Select className="datebox-direction" popupClassName="date-option" suffixIcon={<AiOutlineDown />} defaultValue={relativeDate.period.direction} onChange={changeDirection} >
            <Select.Option className="datebox-option" value="before">{t("campaign.date_before")}</Select.Option>
            <Select.Option className="datebox-option" value="after">{t("campaign.date_after")}</Select.Option>
        </Select></div>
    </>

    return <Field {...rest}>
        <div className='datebox'>
            <div className="datebox datebox-frame">
                <div className="datebox-relative">{contents}</div>
            </div>
        </div>
    </Field>
}