import { useCrud } from "apprise-ui/utils/crudtask"
import { useContext } from "react"
import { useCampaignCalls } from "./calls"
import { CampaignCacheContext } from "./provider"
import { campaignApi, campaignPlural, campaignSingular } from "./constants"
import { usePreload } from 'apprise-frontend-core/client/preload'
import { Campaign } from './model'


export const useCampaignStore = () => {


    const state = useContext(CampaignCacheContext)

    const crud = useCrud({ singular: campaignSingular, plural: campaignPlural })

    const calls = useCampaignCalls()

    const preload = usePreload()

    const self = {

        all: () => state.get().all

        ,

        fetchAll: crud.fetchAllWith(async () => {

            const campaigns = await (preload.get<Campaign[]>(campaignApi) ?? calls.fetchAll())

            state.set(s => {

                s.all = campaigns

            })

            return campaigns

        }).done()

    }

    return self

}