import { SettingsModule } from "apprise-frontend-streams/settings/model";
import { CampaignIcon, campaignType } from "./constants";
import { CampaignSettings } from "./model";
import { CampaignSettingsForm } from "./settings";


export const campaignSettingsModule: SettingsModule<CampaignSettings> = {

    Icon: CampaignIcon,
    Render: CampaignSettingsForm,
    defaults: {},
    type: campaignType,
    name: 'campaign.singular'
}

