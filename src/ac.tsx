import { CampaignsLoader } from "#campaign/provider"
import { defaultConfiguration } from "#config"
import { EventsLoader } from "#event/provider"
import { ModuleInitialiser } from "#modules/initialiser"
import { ProductsLoader } from "#product/provider"
import { RequirementsLoader } from "#requirement/provider"
import { AdminConsoleScaffold } from "#scaffold"
import { VersionUpgradeObserver } from '#utils/upgradeobserver'
import { Application } from "apprise-frontend-core/application"
import { Client } from "apprise-frontend-core/client/client"
import { Preload } from "apprise-frontend-core/client/preload"
import { Configuration } from "apprise-frontend-core/config/configuration"
import { ConfigLoader } from "apprise-frontend-core/config/loader"
import { Intl } from "apprise-frontend-core/intl/intl"
import { vite } from "apprise-frontend-core/utils/vitebridge"
import { Events } from "apprise-frontend-events/events"
import { Login } from "apprise-frontend-iam/login/login"
import { useIamPreloaders } from "apprise-frontend-iam/preloaders"
import { Tenants } from "apprise-frontend-iam/tenant/tenants"
import { Users } from "apprise-frontend-iam/user/users"
import { Mail } from "apprise-frontend-mail/mail"
import { Settings } from "apprise-frontend-streams/settings/settings"
import { Streams } from "apprise-frontend-streams/streams"
import { useTagPreloaders } from "apprise-frontend-tags/preloaders"
import { Tags } from "apprise-frontend-tags/tags"
import { UserInterface } from "apprise-ui/provider"
import { ActiveTimezoneProvider, Timezones } from "apprise-ui/timezone/provider"


export const AdminConsole = () => {

    const loaders = [...useIamPreloaders(), ...useTagPreloaders()]

    return <Application tool={vite}>
        <UserInterface>
            <Configuration config={defaultConfiguration}>
                <Intl>
                    <Client>
                        <Preload loaders={loaders}>
                            <ConfigLoader>
                                <Events>
                                    <Login>
                                        <VersionUpgradeObserver>
                                            <Settings>
                                                <Tags>
                                                    <Tenants>
                                                        <Users>
                                                            <Streams>
                                                                <Mail>
                                                                    <RequirementsLoader>
                                                                        <ProductsLoader>
                                                                            <CampaignsLoader>
                                                                                <EventsLoader>
                                                                                    <Timezones>
                                                                                        <ModuleInitialiser>
                                                                                            <ActiveTimezoneProvider>
                                                                                                <AdminConsoleScaffold />
                                                                                            </ActiveTimezoneProvider>
                                                                                        </ModuleInitialiser>
                                                                                    </Timezones>
                                                                                </EventsLoader>
                                                                            </CampaignsLoader>
                                                                        </ProductsLoader>
                                                                    </RequirementsLoader>
                                                                </Mail>
                                                            </Streams>
                                                        </Users>
                                                    </Tenants>
                                                </Tags>
                                            </Settings>
                                        </VersionUpgradeObserver>
                                    </Login>
                                </Events>
                            </ConfigLoader>
                        </Preload>
                    </Client>
                </Intl>
            </Configuration>
        </UserInterface>
    </Application >
}