
import { useAppSettings } from '#settings/settings'
import { Label, LabelProps } from 'apprise-ui/label/label'
import { DateIcon } from 'apprise-ui/utils/icons'
import { useDateFnsLocale } from 'apprise-ui/utils/localeloader'
import format from 'date-fns/format'


export type DateLabelProps = LabelProps & Partial<{

    date: number | string

    dateFormat: string

    displayMode: 'normal' | 'expiry'

    dateMode: 'settings' | 'short' | 'long'
}>

export const DateLabel = (props: DateLabelProps) => {

    const { date: clientdate, dateFormat, decorations, dateMode = 'settings', icon, ...rest } = props

    const date = typeof clientdate === 'string' ? Date.parse(clientdate) : clientdate

    const locale = useDateFnsLocale()
 
    const dateSettingsFormat = useAppSettings().resolveFormat()

    if (!date)
        return null

    const computedFormat = dateFormat ? dateFormat : (dateMode === 'settings' ? dateSettingsFormat : dateMode === 'short' ? 'PP' : 'PPp')

    let defaultDecorations = decorations

   
    return <Label tip={format(date, 'pp')} icon={icon ?? <DateIcon />} title={format(date, computedFormat, {locale})} decorations={defaultDecorations} {...rest} />
}
