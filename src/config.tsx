
import { RemoteClientConfiguration } from 'apprise-frontend-core/client/config';
import { Config } from 'apprise-frontend-core/config/model';
import { RemoteIntlConfiguration } from 'apprise-frontend-core/intl/config';
import { DeepPartial } from 'apprise-frontend-core/utils/common';
import { RemoteTenantConfiguration } from 'apprise-frontend-iam/tenant/config';
import { RemoteUserConfiguration } from 'apprise-frontend-iam/user/config';
import { tagType } from 'apprise-frontend-tags/constants';



export type AdminConsoleConfig = Config & RemoteIntlConfiguration & RemoteClientConfiguration & RemoteTenantConfiguration & RemoteUserConfiguration

export const defaultConfiguration: DeepPartial<AdminConsoleConfig> = {

    latest: true,

    intl: {
        supportedLanguages: ['en', 'fr'],
        requiredLanguages: ['en', 'fr']
    }
    ,


    authz: {
        enableMultiManagers: true
    }

    ,

    client: {

        mocks: { responseDelay: 150 },
        request: {
            timeout: 15000
        },
        services: { domain: { label: "domain", prefix: "/domain", default: true } }

    }

    ,

    user: {

        loadSummaries:false

    }


    ,


    propertyExcludes: {

        [tagType]: ['value']
    }

    ,


    dateFormats: {

        short: {
            name: 'rav.dates_short',
            format: 'd/M/yy'
        },
        medium: {
            name: 'rav.dates_medium',
            format: 'dd/MM/yy'
        },
        full: {
            name: 'rav.dates_full',
            format: 'dd/MM/yyyy'
        }

    }

}