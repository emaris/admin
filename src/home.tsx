import { useT } from 'apprise-frontend-core/intl/language'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { Button } from 'apprise-ui/button/button'
import { Page } from 'apprise-ui/page/page'
import './home.scss'
import { UserIcon } from 'apprise-frontend-iam/user/constants'
import { userRoute } from 'apprise-frontend-iam/user/routing'
import { TenantIcon } from 'apprise-frontend-iam/tenant/constants'
import { tenantRoute } from 'apprise-frontend-iam/tenant/routing'
import { TagIcon } from 'apprise-frontend-tags/constants'
import { tagSystemRoute } from 'apprise-frontend-tags/tag/routing'
import { DotIcon } from 'apprise-ui/utils/icons'
import { cloneElement } from 'react'
import { utils } from 'apprise-frontend-core/utils/common'
import { useToolBridge } from 'apprise-frontend-core/utils/toolbridge'


export const Home = () => {

    const t = useT()

    const logged = useTenancyOracle()

    const { absoluteOf } = useToolBridge()


    const subtitle = logged.hasNoTenant() ? t("ac_home.full_subtitle") :
        logged.isTenantManager() ?
            t("ac_home.manager_subtitle")
            : t("ac_home.manager_subtitle")

    const usersBlurb = <Blurb key={'usersBlurb'} color='darkgoldenrod' icon={<UserIcon />} className="dashboard" title={t("ac_home.user_title")}>
        <ul>
            <li><DotIcon />{t("ac_home.user_description_1")}</li>
            <li><DotIcon />{t("ac_home.user_description_2")}</li>
            <li><DotIcon />{t("ac_home.user_description_3")}</li>
            <li><DotIcon />{t("ac_home.user_description_4")}</li>
        </ul>
        <Button linkTo={userRoute}>{t("ac_home.user_button_1")}</Button>
        <Button enabled={logged.isTenantManager() || logged.hasNoTenant()} linkTo={`${userRoute}/new`} >{t("ac_home.user_button_2")}</Button>
    </Blurb>

    const tenantsBlurb = <Blurb key={'tenantsBlurb'} color='#0895c3' icon={<TenantIcon />} title={t("ac_home.tenant_title")}>
        <ul>
            <li><DotIcon />{t("ac_home.tenant_description_1")}</li>
            <li><DotIcon />{t("ac_home.tenant_description_2")}</li>
        </ul>
        <Button linkTo={tenantRoute}>{t("ac_home.tenant_button_1")}</Button>
        <Button linkTo={`${tenantRoute}/new`} >{t("ac_home.tenant_button_2")}</Button>
    </Blurb>

    const tenantsManagerBlurb = <Blurb key={'tenantsManagerBlurb'} color='teal' icon={<TenantIcon />} title={t("ac_home.tenant_manager_title")}>
        <ul>
            <li><DotIcon />{t("ac_home.tenant_manager_description_1")}</li>
            <li><DotIcon />{t("ac_home.tenant_manager_description_2")}</li>
            <li><DotIcon />{t("ac_home.tenant_manager_description_3")}</li>
            <li><DotIcon />{t("ac_home.tenant_manager_description_4")}</li>
        </ul>
        <Button linkTo={tenantRoute}>{t("ac_home.tenant_manager_button_1")}</Button>
    </Blurb>

    const settingsBlurb = <Blurb key={'settingsBlurb'} color='lightseagreen' icon={<TagIcon />} title={t("ac_home.settings_title")}>
        <ul>
            <li><DotIcon />{t("ac_home.settings_description_1")}</li>
            <li><DotIcon />{t("ac_home.settings_description_2")}</li>
            <li><DotIcon />{t("ac_home.settings_description_3")}</li>
            <li><DotIcon />{t("ac_home.settings_description_4")}</li>
        </ul>
        <Button linkTo={tagSystemRoute}>{t("ac_home.settings_button_1")}</Button>
    </Blurb>

    const blurbs = logged.hasNoTenant() ? [tenantsBlurb, usersBlurb, settingsBlurb] : [tenantsManagerBlurb, usersBlurb]



    return <Page className='ac-home custom-scrollbar dark-custom-scrollbar' fullBleed >

        <div className="home-title">
            <img className="home-ico" alt="logo" src={absoluteOf('/images/aclogo.png')} />
            <div className="app-name">{t("ac_home.title")}</div>
            <div className="app-subtitle">{subtitle}</div>
        </div>

        <div className="home-blurb">
            {blurbs}
        </div>

    </Page>

}


type HomeEntryProps = React.PropsWithChildren<{
    className?: string
    icon: JSX.Element
    color?: string
    title: string
    buttons?: JSX.Element[]
}>

const Blurb = (props: HomeEntryProps) => {

    const { className, icon, color, title } = props

    const children = utils().elementsIn(props.children)

    const btns = children.filter(utils().isElementOf(Button))
    const other = children.filter(ele => !utils().isElementOf(Button)(ele))

    return <div className={`entry ${className ?? ''}`}>
        <div className="innerEntry">
            <div className="logo">
                <div style={{ color }} className="icon-halo">
                    {icon}
                </div>
            </div>
            <div className="contents">
                <div className="title">{title}</div>
                <div className="text">
                    {/* <div className="subtitle">{subtitle}</div> */}
                    {other}
                </div>
                {btns && <div className="buttons">{btns.map((b, i) => cloneElement(b, { className: "button", size: 'large', innerStyle: { background: color, borderColor: color, color: 'white' }, key: i }))} </div>}
            </div>
        </div>
    </div >


}