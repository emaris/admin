


import { Crumb } from 'apprise-ui/scaffold/scaffold';
import React from 'react';

import logobar from './logobar.png';


export const useCrumbs = () => {
   

    return <React.Fragment>
    
        <Crumb path="/"><img alt='logo' height={18} src={logobar} /></Crumb>
    
    </React.Fragment>

}

