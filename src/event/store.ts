import { usePreload } from 'apprise-frontend-core/client/preload'
import { useCrud } from "apprise-ui/utils/crudtask"
import { useContext } from "react"
import { useEventCalls } from "./calls"
import { eventApi, eventPlural, eventSingular } from "./constants"
import { Event } from './model'
import { EventCacheContext } from "./provider"


export const useEventStore = () => {


    const state = useContext(EventCacheContext)

    const crud = useCrud({ singular: eventSingular, plural: eventPlural })

    const calls = useEventCalls()

    const preload = usePreload()

    const self = {

        all: () => state.get().all

        ,

        fetchAll: crud.fetchAllWith(async () => {

            const events = await (preload.get<Event[]>(eventApi) ?? calls.fetchAll())

            state.set(s => {

                s.all = events

            })

            return events

        }).done()

    }

    return self

}