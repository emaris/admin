import { any } from "apprise-frontend-core/authz/constants"
import { useL } from "apprise-frontend-core/intl/multilang"
import { useTenancyOracle } from "apprise-frontend-iam/authz/tenant"
import { PermissionBox } from "apprise-frontend-iam/permission/permissionbox"
import { User } from "apprise-frontend-iam/user/model"
import { UserPermissionBoxProps, UserPermissionModule } from "apprise-frontend-iam/user/modules"
import { Label } from "apprise-ui/label/label"
import { Event } from "./model"
import { useEventStore } from "./store"
import { EventIcon, eventIcon, eventPlural, eventSingular, eventType } from "./constants"


export const baseEventAction = { icon: <EventIcon />, type: eventType, resource: any }

export const eventActions = {  

    manage: {...baseEventAction,  labels:["manage"], shortName:"actions.event_manage_short",name:"actions.event_manage_name", description: "actions.event_manage_desc"},
}

export const useEventPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: eventIcon,
        type: eventType,
        plural: eventPlural,

        box: props => oracle.given(props.edited).hasNoTenant() && <EventPermissionBox {...props} />,

        initial: (user: User) => {

            const u = oracle.given(user)

            return u.hasNoTenant() ? [eventActions.manage] : []

        }

    } as UserPermissionModule<Event>

}


export const EventPermissionBox = (props: Partial<UserPermissionBoxProps<Event>>) => {

    const { edited, ...rest } = props

    const l = useL()

    const store = useEventStore()

    const actions = eventActions

    return <PermissionBox<Event>

        resourceSingular={eventSingular}

        permissionRange={Object.values(actions)}
        maxPrivilege={actions.manage ?? {}}

        resourcePlural={eventPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<EventIcon />} title={l(s.name)} />}
        resourceRange={store.all()}


        wildcard
        preselectWildcard


        {...rest} />
}