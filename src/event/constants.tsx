import { AiOutlineClockCircle } from "react-icons/ai"

export const eventApi = '/event'

export const eventSingular = "event.singular"
export const eventPlural = "event.plural"
export const eventType= "event"
export const eventIcon = AiOutlineClockCircle
export const EventIcon = AiOutlineClockCircle