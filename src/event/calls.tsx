import { useCall } from "apprise-frontend-core/client/call"
import { Event } from "./model"
import { eventApi } from "./constants"

export const useEventCalls = () => {

    const call = useCall()

    const self = {

        fetchAll: () => call.at(eventApi).get<Event[]>()

    }

    return self

}