import { State } from "apprise-frontend-core/state/api";
import { StateProvider } from "apprise-frontend-core/state/provider";
import { useRenderGuard } from "apprise-frontend-core/utils/renderguard";
import { PropsWithChildren, createContext } from "react";
import { Event } from "./model";
import { useEventStore } from "./store";

export type EventCache = {
    all: Event[]
}

export const initialCacheState: EventCache = {
    all: []
}

export const EventCacheContext = createContext<State<EventCache>>(undefined!)

const EventProvider = (props: PropsWithChildren) => {

    return <StateProvider initialState={initialCacheState} context={EventCacheContext} >
        {props.children}
    </StateProvider>


}

const EventLoader = (props: PropsWithChildren) => {

    const { children } = props

    const store = useEventStore()

    const activate = () => store.fetchAll()


    const { content } = useRenderGuard({

        render: children,
        orRun: activate
        
    })

    return content

}

export const EventsLoader = (props: PropsWithChildren) => {

    const { children } = props

    return <EventProvider>
        <EventLoader>
            {children}
        </EventLoader>
    </EventProvider>

}