import { AiFillSafetyCertificate, AiFillAppstore, AiFillContainer } from "react-icons/ai"
import { RiHome7Line } from "react-icons/ri"

export const appColor = '#0895c3'

export const AppIcon = <AiFillSafetyCertificate size={22}  />

export const HomeIcon = RiHome7Line

export const mcColor ="#0e9785"
export const McIcon = <AiFillAppstore size={22} color={mcColor} />
export const mcRoute = "/mc"

export const rcColor ="#0ca4b8"
export const RcIcon = <AiFillContainer size={22} color={rcColor} />
export const rcRoute = "/rc"