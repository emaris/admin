import { FiBox } from 'react-icons/fi'

export const requirementApi = '/requirement'

export const requirementSingular = "requirement.singular"
export const requirementPlural = "requirement.plural"

export const requirementType = "requirement"
export const requirementSubmissionType = "requirement-submission"
export const requirementIcon = FiBox
export const RequirementIcon = FiBox
export const requirementRoute = "/requirement"

export const requirementSubmissionSingular = "submission.requirement_singular"
export const requirementSubmissionPlural = "submission.requirement_plural"
