import { any } from "apprise-frontend-core/authz/constants"
import { useL } from "apprise-frontend-core/intl/multilang"
import { useTenancyOracle } from "apprise-frontend-iam/authz/tenant"
import { PermissionBox, PermissionBoxProps } from "apprise-frontend-iam/permission/permissionbox"
import { UserPermissionModule } from "apprise-frontend-iam/user/modules"
import { Label } from "apprise-ui/label/label"
import { RequirementIcon, requirementIcon, requirementPlural, requirementSingular, requirementSubmissionPlural, requirementSubmissionSingular, requirementSubmissionType, requirementType } from "./constants"
import { Requirement } from "./model"
import { useRequirementStore } from "./store"



const baseRequirementAction = { icon:<RequirementIcon />, type: requirementType, resource :any }


export const requirementAdminActions = {
    manage:  {...baseRequirementAction, labels:["manage"], shortName:"actions.requirement_manage_short",name:"actions.requirement_manage.name", description: "actions.requirement_manage_desc"}
    ,
    edit:  {...baseRequirementAction, labels:["manage","edit"], shortName:"actions.requirement_edit_short",name:"actions.requirement_edit_name", description: "actions.requirement_edit_desc"}
    ,
    assess:  {...baseRequirementAction, labels:["manage","assess"], shortName:"actions.requirement_assess_short",name:"actions.requirement_assess_name", description: "actions.requirement_assess_desc"}
}

export const requirementActions = requirementAdminActions


export const useRequirementPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: requirementIcon,
        type: requirementType,
        plural: requirementPlural,

        box: props => oracle.given(props.edited).hasNoTenant() && <RequirementPermissionBox {...props} />

    } as UserPermissionModule<Requirement>

}

export const RequirementPermissionBox = (props: Partial<PermissionBoxProps<Requirement>>) => {

    const { ...rest } = props

    const l = useL()

    const store = useRequirementStore()

    const actions = requirementAdminActions

    return <PermissionBox<Requirement>

        resourceSingular={requirementSingular}

        permissionRange={Object.values(actions)}
        maxPrivilege={actions.manage ?? {}}

        resourcePlural={requirementPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<RequirementIcon />} title={l(s.name)} />}
        resourceRange={store.all()}

        
        wildcard
        preselectWildcard


        {...rest} />
}



const baseRequiremenSubmissiontAction = { icon:<RequirementIcon />, type: requirementSubmissionType, resource :any }

export const requirementSubmissionActions = {
    
    manage:  {...baseRequiremenSubmissiontAction, labels:["manage"], shortName:"actions.requirement_submission_manage_short",name:"actions.requirement_submission_manage_name", description: "actions.requirement_submission_manage_desc"}
    ,
    edit:  {...baseRequiremenSubmissiontAction, labels:["manage","edit"], shortName:"actions.requirement_submission_edit_short",name:"actions.requirement_submission_edit_name", description: "actions.requirement_submission_edit_desc"}
    ,
    submit:  {...baseRequiremenSubmissiontAction, labels:["manage","submit"], shortName:"actions.requirement_submission_submit_short",name:"actions.requirement_submission_submit_name", description: "actions.requirement_submission_submit_desc"}
    ,
    message:  {...baseRequiremenSubmissiontAction, labels:["manage","message"], shortName:"actions.requirement_submission_message_short",name:"actions.requirement_submission_message_name", description: "actions.requirement_submission_message_desc"}
}


export const useRequirementSubmissionPermissionModule = () => {

    const oracle = useTenancyOracle()

    return {

        Icon: RequirementIcon,
        type: 'requirement-submission',
        plural: requirementSubmissionPlural,

        box: props => oracle.given(props.edited).isTenantUser() && <RequirementSubmissionPermissionBox {...props} />

    } as UserPermissionModule<Requirement>

}

export const RequirementSubmissionPermissionBox = (props: Partial<PermissionBoxProps<Requirement>>) => {

    const { ...rest } = props

    const l = useL()

    const store = useRequirementStore()

    return <PermissionBox<Requirement>

        resourceSingular={requirementSubmissionSingular}

        permissionRange={Object.values(requirementSubmissionActions)}
        maxPrivilege={requirementSubmissionActions.manage ?? {}}

        resourcePlural={requirementSubmissionPlural}
        resourceId={s => s.id}
        resourceRender={s => <Label icon={<RequirementIcon />} title={l(s.name)} />}
        resourceRange={store.all()}

        tenantResource

        
        wildcard
        preselectWildcard


        {...rest} />
}