import { useCrud } from "apprise-ui/utils/crudtask"
import { useContext } from "react"
import { RequirementCacheContext } from "./provider"
import { useRequirementCalls } from "./calls"
import { requirementApi, requirementPlural, requirementSingular } from "./constants"
import { usePreload } from 'apprise-frontend-core/client/preload'
import { Requirement } from './model'


export const useRequirementStore = () => {


    const state = useContext(RequirementCacheContext)

    const crud = useCrud({ singular: requirementSingular, plural: requirementPlural })

    const calls = useRequirementCalls()

    const preload = usePreload()

    const self = {

        all: () => state.get().all

        ,

        fetchAll: crud.fetchAllWith(async () => {

            const requirements = await (preload.get<Requirement[]>(requirementApi) ?? calls.fetchAll())

            state.set(s => {

                s.all = requirements

            })

            return requirements

        }).done()

    }

    return self

}