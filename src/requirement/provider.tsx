import { Requirement } from "#requirement/model";
import { State } from "apprise-frontend-core/state/api";
import { StateProvider } from "apprise-frontend-core/state/provider";
import { useRenderGuard } from "apprise-frontend-core/utils/renderguard";
import { PropsWithChildren, createContext } from "react";
import { useRequirementStore } from "./store";

export type RequirementCache = {
    all: Requirement[]
}

export const initialCacheState: RequirementCache = {
    all: []
}

export const RequirementCacheContext = createContext<State<RequirementCache>>(undefined!)

const RequirementProvider = (props: PropsWithChildren) => {

    return <StateProvider initialState={initialCacheState} context={RequirementCacheContext} >
        {props.children}
    </StateProvider>


}

const RequirementLoader = (props: PropsWithChildren) => {

    const { children } = props

    const store = useRequirementStore()

    const activate = () => store.fetchAll()


    const { content } = useRenderGuard({

        render: children,
        orRun: activate
        
    })

    return content

}

export const RequirementsLoader = (props: PropsWithChildren) => {

    const { children } = props

    return <RequirementProvider>
        <RequirementLoader>
            {children}
        </RequirementLoader>
    </RequirementProvider>

}