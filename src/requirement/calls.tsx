import { Requirement } from "#requirement/model"
import { useCall } from "apprise-frontend-core/client/call"
import { requirementApi } from "./constants"

export const useRequirementCalls = () => {

    const call = useCall()

    const self = {

        fetchAll: () => call.at(`${requirementApi}/raw`).get<Requirement[]>()

    }

    return self

}