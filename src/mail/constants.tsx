import { MdMail } from 'react-icons/md'

export const mailType = "mail" 
export const mailIcon = MdMail
export const MailIcon = MdMail

export const noMailTopic = {key: 'mail.no_topic', value: 'none'}

export const mailSingular = 'mail.singular'
export const mailPlural = mailSingular

export const mailSubject = 'mail'
export const anyTopic = '*'
export const anyTarget = '*'
export const mailTopicSeparator = '.'

export const allMessageMailTopic = 'message'

export const subchangeMailTopic = 'submission'
export const subAssessedMailTopic = 'submission.assessed'
export const subAssessementRevokedMailTopic = 'submission.assessed.revoked'
export const subSubmittedMailTopic = 'submission.submitted'
export const subPendingApprovalMailTopic = 'submission.pendingapproval'
export const subRequestForChangeMailTopic = 'submission.requestchange'
export const subPublishedMailTopic = 'submission.published'
export const subSharedMailTopic = 'submission.shared'
export const dueDateMailTopic = 'event'
export const dueDateReminderMailTopic = 'event.reminder'

export const tenantMailTopic='tenant'
export const tenantActiveMailTopic='tenant.active'
export const tenantInactiveMailTopic='tenant.inactive'