import { tenantActions } from "apprise-frontend-iam/authz/oracle";
import { allMessageMailTopic, dueDateMailTopic, dueDateReminderMailTopic, subAssessedMailTopic, subPendingApprovalMailTopic, subRequestForChangeMailTopic, subSharedMailTopic, subSubmittedMailTopic, subchangeMailTopic, tenantActiveMailTopic, tenantInactiveMailTopic, tenantMailTopic } from "./constants";
import { MailModule } from "apprise-frontend-mail/modules";
import { MailTopic } from "apprise-frontend-mail/model";

export const topics:MailTopic[] = [

    {key: 'message.all_topic', value: allMessageMailTopic},
    {key: 'campaign.mail_topic_due', value: dueDateMailTopic},
    {key: 'campaign.mail_topic_due_reminder', value: dueDateReminderMailTopic},
    {key: 'campaign.mail_topic_submission', value: subchangeMailTopic},
    {key: 'campaign.mail_topic_submission_pendingapproval', value: subPendingApprovalMailTopic},
    {key: 'campaign.mail_topic_submission_requestchanges', value: subRequestForChangeMailTopic},
    {key: 'campaign.mail_topic_submission_submitted', value: subSubmittedMailTopic},
    {key: 'campaign.mail_topic_submission_assessed', value: subAssessedMailTopic},
    {key: 'campaign.mail_topic_submission_shared', value: subSharedMailTopic},
    {key: 'tenant.mail_topic_all', value: tenantMailTopic, audience:[ tenantActions.manage ] },
    {key: 'tenant.mail_topic_active', value: tenantActiveMailTopic, audience:[ tenantActions.manage ] },
    {key: 'tenant.mail_topic_inactive', value: tenantInactiveMailTopic, audience:[ tenantActions.manage ] }

]

export const emarisMailModule: MailModule = {

    name: 'rav',
    topics

}