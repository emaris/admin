import { useLanguage } from 'apprise-frontend-core/intl/language'
import { useToolBridge } from 'apprise-frontend-core/utils/toolbridge'





export const IOTCLink = () => {

    const currentLanguage = useLanguage().current()

    const { absoluteOf } = useToolBridge()

    const link = `https://www.iotc.org${currentLanguage === 'en' ? '' : `/${currentLanguage}`}`

    return <a href={link} target='_blank' rel="noreferrer">
        <img alt="iotc" src={absoluteOf('/images/iotc.png')} height={22} />
    </a>
}