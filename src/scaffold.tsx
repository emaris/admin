import { AppIcon, HomeIcon, McIcon, RcIcon, appColor, mcRoute, rcRoute } from '#constants'
import { useCrumbs } from '#crumbs'
import { AdminConsoleHeader } from '#header'
import { IOTCLink } from '#iotc'
import { useT } from 'apprise-frontend-core/intl/language'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { useIamSections } from 'apprise-frontend-iam/sections'
import { tenantType } from 'apprise-frontend-iam/tenant/constants'
import { useTenantCrumbs } from 'apprise-frontend-iam/tenant/crumbs'
import { useUserCrumbs } from 'apprise-frontend-iam/user/crumbs'
import { tagType } from 'apprise-frontend-tags/constants'
import { useTagCrumbs } from 'apprise-frontend-tags/crumbs'
import { useTagSection } from 'apprise-frontend-tags/section'
import { RoutableTargets } from 'apprise-ui/link/routabletargets'
import { Scaffold, Section, SideLink } from 'apprise-ui/scaffold/scaffold'
import { Home } from './home'


export const AdminConsoleScaffold = () => {

    const t = useT()

    const crumbs = useCrumbs()

    const oracle = useTenancyOracle()
    
    const noTenant = oracle.hasNoTenant()

    const excludes = noTenant ? [] : oracle.managesMultipleTenants() ? [tagType] : [tenantType, tagType]

    const tagSection = useTagSection()

    return <RoutableTargets exclude={excludes}>

            <Scaffold icon={AppIcon} color={appColor} title={t('ac_home.title')} sidebar header={<AdminConsoleHeader />}>

                {useTagCrumbs()}
                {useUserCrumbs()}
                {useTenantCrumbs()}

                {crumbs}

                <Section icon={<HomeIcon />} title='Home' exact route='/'>
                    <Home />
                </Section>

                {useIamSections()}

                {noTenant && tagSection}


                <SideLink>
                    <IOTCLink />
                </SideLink>

                <SideLink>
                    <ReturnToDashboardIcon />
                </SideLink>


            </Scaffold>

    </RoutableTargets>

}

const ReturnToDashboardIcon = () => {


    const logged = useTenancyOracle()

    const hasNoTenant = logged.hasNoTenant()

    const Icon = hasNoTenant ? McIcon : RcIcon
    const route = hasNoTenant ? mcRoute : rcRoute

    return <a href={route}>{Icon}</a>

}


