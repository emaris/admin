

import { AppSettings } from '#settings/settings'
import { SystemSettingsPanel } from '#settings/system'
import { SettingsModule } from 'apprise-frontend-streams/settings/model'
import { AiFillAppstore } from 'react-icons/ai'



export const systemSettingsModule: SettingsModule<AppSettings> = {

    Icon: AiFillAppstore,
    Render: SystemSettingsPanel,
    defaults: {
        dateFormat: 'medium'
    },
    type: 'system',
    name: 'appsettings.name'
}


export const useValidationSettings = () : SettingsModule<AppSettings> => {

    return {

        Icon: undefined!,
        Render: undefined!,
        defaults: {},
        type: undefined!,
        name: ''
    }
}


