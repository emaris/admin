import { DashboardIcon } from "#campaign/constants"
import { calendarOptions, dashboardOptions } from "#campaign/model"
import { useT } from "apprise-frontend-core/intl/language"
import { utils } from "apprise-frontend-core/utils/common"
import { FormState } from "apprise-frontend-core/utils/form"
import { DownloadOptions } from "apprise-frontend-iam/user/download"
import { User } from "apprise-frontend-iam/user/model"
import { UserPreferencesModule } from "apprise-frontend-iam/user/modules"
import { MailTopicBox } from "apprise-frontend-mail/mailtopicbox"
import { MailTopic } from "apprise-frontend-mail/model"
import { SheetWriter } from "apprise-frontend-parse/workbookWriter"
import { useValidation } from "apprise-ui/field/validation"
import { Label } from "apprise-ui/label/label"
import { SelectBox } from "apprise-ui/selectbox/selectbox"
import { TextBox } from "apprise-ui/textbox/textbox"
import { TimezoneModule, useActiveTimezone, useTimezonesRegistry } from "apprise-ui/timezone/api"
import { homeTimezoneType } from "apprise-ui/timezone/provider"
import { ZonePickerBox } from "apprise-ui/timezone/zonepicker"
import { Fragment } from "react"
import { AiOutlineCalendar, AiOutlineUnorderedList } from "react-icons/ai"

export type EmarisUserMailProfile = {

    // these are set in the user form.
    mailProfile: {

        topics: MailTopic[]
    }
}

export type EmarisUserPreferences = EmarisUserMailProfile

const defaultPreferences: EmarisUserMailProfile = {

    mailProfile: {

        topics: []
    }
}

const useFields = (props: FormState<User>) => {

    const t = useT()
    const { check, is } = useValidation()

    const { edited } = props

    return {

        'timezone': {
            label: t('campaign.location_lbl'),
            msg: t('campaign.location_msg'),
            help: t("campaign.location_help")
        }

        ,

        'preferredTimezone': {
            label: t('campaign.default_location_lbl'),
            msg: t('campaign.default_location_msg'),
            help: <>
                {t("campaign.default_location_help1")}<br />
                {t("campaign.default_location_help2")}
            </>
        }

        ,

        'approveCycle': {
            label: t('campaign.approve_cycle_lbl'),
            msg: t('campaign.approve_cycle_msg'),
            help: <>
                {t("campaign.approve_cycle_help1")}<br />
                {t("campaign.approve_cycle_help2")}
            </>
        }

        ,

        'email': {
            label: t('user.alt_email_lbl'),
            msg: t('user.alt_email_msg'),
            help: t("user.alt_email_help"),

            ... (edited.details?.preferences.email && edited.details.email !== '') && check(is.empty).and(is.badEmail).on(edited.details.preferences.email)
        }

        ,

        'dashboardView': {
            label: t('user.default_dashboard_lbl'),
            msg: t('user.default_dashboard_msg'),
            help: <>
                {t("user.default_dashboard_help1")}<br />
                {t("user.default_dashboard_help2")}
            </>
        }

        ,

        'calendarView': {
            label: t('user.default_calendar_lbl'),
            msg: t('user.default_calendar_msg'),
            help: <>
                {t("user.default_calendar_help1")}<br />
                {t("user.default_calendar_help2")}
            </>
        }
    }
}

const Form = (props: FormState<User>) => {

    const { edited, set } = props

    const t = useT()

    const report = useFields(props)

    const dashboardViewOptions = Object.keys(dashboardOptions)

    const calendarViewOptions = Object.keys(calendarOptions)

    const prefs = utils().merge(defaultPreferences, edited.details.preferences) as EmarisUserPreferences

    const activeTimezoneCtx = useActiveTimezone()
    const timezoneRegistry = useTimezonesRegistry()

    const onTimezoneChange = (location: string | undefined) => {

        const homeTimezone = timezoneRegistry.all().find(module => module.name === homeTimezoneType)

        // Modify active timezone and registry to reflect the new home timezone
        if (homeTimezone) {

            const newHomeTimezoneModule = { ...homeTimezone, value: () => location ?? '' }

            if (activeTimezoneCtx.get()?.name === homeTimezoneType) {
                activeTimezoneCtx.reset(newHomeTimezoneModule)
            }

            timezoneRegistry.alter(newHomeTimezoneModule)

        }

        set.with<string>((u, v) => u.details.preferences.location = v)(location)
    }

    const onPreferredTimezoneChange = (v: TimezoneModule | undefined) => {
        activeTimezoneCtx.reset(v!)
        set.with<string>((u, v) => u.details.preferences.dateLocation = v)(v?.name)
    }

    return <Fragment>

        <ZonePickerBox
            info={report.timezone}
            includeLocal={false}
            onChange={v => onTimezoneChange(v?.option)}
        >
            {edited.details.preferences.location}
        </ZonePickerBox>

        <SelectBox<TimezoneModule>
            keyOf={tz => tz.name}
            label={report.preferredTimezone.label}
            info={report.preferredTimezone}
            options={timezoneRegistry.all()}
            onChange={onPreferredTimezoneChange}
            render={pt => <Label noIcon title={t('time.preferred_timezone_name', { value: pt.label })} />}>
            {timezoneRegistry.all().find(tz => tz.name === edited.details.preferences.dateLocation)}
        </SelectBox>

        <SelectBox<string>
            info={report.dashboardView}
            options={dashboardViewOptions}
            onChange={set.with((u, v) => u.details.preferences.defaultDashboardView = v)}
            render={dv => <Label icon={<DashboardIcon />} title={t(dashboardOptions[dv])} />}>
            {edited.details.preferences.defaultDashboardView}
        </SelectBox>

        <SelectBox<string>
            info={report.calendarView}
            options={calendarViewOptions}
            onChange={set.with((u, v) => u.details.preferences.defaultCalendarView = v)}
            render={cv => <Label icon={cv === 'month' ? <AiOutlineCalendar /> : <AiOutlineUnorderedList />} title={t(calendarOptions[cv])} />}>
            {edited.details.preferences.defaultCalendarView}
        </SelectBox>


        <MailTopicBox onChange={set.with((m, v) => {

            const profile: EmarisUserPreferences = { mailProfile: { ...prefs.mailProfile, topics: v } }

            m.details.preferences = { ...prefs, ...profile }

        })}>
            {prefs.mailProfile?.topics}
        </MailTopicBox>

        <TextBox info={report.email} onChange={set.with((u, v) => u.details.preferences.email = v)}>{edited.details.preferences.email}</TextBox>

    </Fragment>

}

export const useUserPreferenceModule = (): UserPreferencesModule => {

    const t = useT()

    return {

        fields: useFields,
        Form,
        download: (writer: SheetWriter<User>, _: DownloadOptions) => {

            writer.text(t('user.alt_email_lbl')).render(u => u.details.preferences.email)

        }
    }
}