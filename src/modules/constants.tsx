
import { RiCheckboxMultipleBlankLine } from 'react-icons/ri'

export const complianceType="compliance"
export const complianceSingular = "campaign.compliance_singular"

export const SubmissionIcon = RiCheckboxMultipleBlankLine

export const capCategoryId = 'TGC-custom-asset-property'

export const capTypeProp = 'cap_type'
export const capDefaultProp = 'cap_default'
export const complianceValueProp = 'compliance-value'
export const timelinessValueProp = 'timeliness-value'
export const allTypePropValue = 'all'