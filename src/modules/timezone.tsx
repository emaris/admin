import { campaignType } from "#campaign/constants"
import { useT } from "apprise-frontend-core/intl/language"
import { useLogged } from "apprise-frontend-iam/login/logged"
import { TimezoneModule } from "apprise-ui/timezone/api"
import { useCurrentTimezone, useHomeTimezone, useUTCTimezone } from "apprise-ui/timezone/provider"
import { FiLayers } from "react-icons/fi"


export const useAdminConsoleTimezones = () => {

    const logged = useLogged()

    const t = useT()


    const defaultValue = logged.details.preferences.dateLocation ?? 'current'


    const currentTimezoneModule = useCurrentTimezone()
    const universalTimezoneModule = useUTCTimezone()
    const homeTimezoneModule = useHomeTimezone()
        
    const universalTimezone = {
        ...universalTimezoneModule,
        label: t('time.universal_timezone'),
        default: defaultValue === universalTimezoneModule.name
    }

    const homeTimezone: TimezoneModule = {
        ...homeTimezoneModule,
        value: () => logged.details.preferences.location || Intl.DateTimeFormat().resolvedOptions().timeZone,
        default: defaultValue === homeTimezoneModule.name
    }

    const campaignTimezone: TimezoneModule = {
        value: currentTimezoneModule.value,
        name: campaignType,
        label: t('time.campaign_timezone'),
        icon: FiLayers
    }

    const currentTimezone: TimezoneModule = {
        ...currentTimezoneModule,
        default: defaultValue === currentTimezoneModule.name
    }

    return [currentTimezone, homeTimezone, campaignTimezone, universalTimezone]

}