

import { campaignSettingsModule } from '#campaign/module'
import { useCampaignPermissionModule } from '#campaign/permission'
import { useEventPermissionModule } from '#event/permission'
import { emarisMailModule } from '#mail/mail'
import { useProductPermissionModule, useProductSubmissionPermissionModule } from '#product/permission'
import { useRequirementPermissionModule, useRequirementSubmissionPermissionModule } from '#requirement/permission'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { useIAMSettingsModule } from 'apprise-frontend-iam/settings'
import { useTenantPreferencesModules } from 'apprise-frontend-iam/tenant/modules'
import { tenantTagPropertiesModule } from 'apprise-frontend-iam/tenant/tagprops'
import { useUserPermissionModules, useUserPreferencesModules } from 'apprise-frontend-iam/user/modules'
import { useMailModules } from 'apprise-frontend-mail/modules'
import { mailSettingsModule } from 'apprise-frontend-mail/settings'
import { useSettingsModules } from 'apprise-frontend-streams/settings/api'
import { useCategoryStore } from 'apprise-frontend-tags/category/store'
import { useCustomTagProperties } from 'apprise-frontend-tags/customprop/api'
import { useTagModules } from 'apprise-frontend-tags/tag/modules'
import { useTimezonesRegistry } from 'apprise-ui/timezone/api'
import { PropsWithChildren, useEffect } from 'react'
import { systemSettingsModule } from './settings'
import { assetTagModule, campaignTagModule, complianceTagModule, otherTagModule, productTagModule, requirementTagModule, submissionTagModule, systemTagModule, timelinessTagModule, useCapCategory, useCustomPropertiesTypes } from './tag'
import { tenantPreferencesModule } from './tenants'
import { useAdminConsoleTimezones } from './timezone'
import { useUserPreferenceModule } from './users'


export type InitialiserProps = PropsWithChildren

export const ModuleInitialiser = (props: InitialiserProps) => {

    const { children } = props

    const userPermissionModules = useUserPermissionModules()

    const settingsModules = useSettingsModules()

    const tagModules = useTagModules()
    const customprops = useCustomPropertiesTypes()
    const tagProperties = useCustomTagProperties()
    const capCategory = useCapCategory()
    const campaignPermissions = useCampaignPermissionModule()
    const requirementPermissions = useRequirementPermissionModule()
    const productPermissions = useProductPermissionModule()

    const requirementSubmissionPermissions = useRequirementSubmissionPermissionModule()
    const productSubmissionPermissions = useProductSubmissionPermissionModule()

    const userPreferencesModules = useUserPreferencesModules()
    const tenantPreferencesModules = useTenantPreferencesModules()

    const eventPermissions = useEventPermissionModule()

    const mailModules = useMailModules()

    const timezoneModules = useTimezonesRegistry()

    const acTimezones = useAdminConsoleTimezones()

    const catstore = useCategoryStore()

    const userPreferencesModule = useUserPreferenceModule()

    const iamSettingsModule = useIAMSettingsModule()

    // auto-stages for convenience.
    useEffect(() => {

        if (!catstore.lookupCategory(capCategory.id))
            catstore.saveCategory(capCategory)

    // eslint-disable-next-line
    }, [])

    const activate = () => {

        settingsModules.register(systemSettingsModule)
        settingsModules.register(campaignSettingsModule)
        settingsModules.register(mailSettingsModule)
        settingsModules.register(iamSettingsModule)

        tagModules.register(systemTagModule)
        tagModules.register(campaignTagModule)
        tagModules.register(complianceTagModule)
        tagModules.register(requirementTagModule)
        tagModules.register(productTagModule)
        tagModules.register(timelinessTagModule)
        tagModules.register(submissionTagModule)
        tagModules.register(assetTagModule)
        tagModules.register(otherTagModule)

        tagProperties.register(tenantTagPropertiesModule, ...customprops)

        userPermissionModules.register(campaignPermissions)
        userPermissionModules.register(requirementPermissions)
        userPermissionModules.register(productPermissions)
        userPermissionModules.register(requirementSubmissionPermissions)
        userPermissionModules.register(productSubmissionPermissions)
        userPermissionModules.register(eventPermissions)

        userPreferencesModules.register(userPreferencesModule)
        tenantPreferencesModules.register(tenantPreferencesModule)

        mailModules.register(emarisMailModule)

        timezoneModules.register(...acTimezones)

    }

    const { content } = useRenderGuard({

        render: children,
        orRun: activate
    })

    return content
}