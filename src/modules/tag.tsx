
import { campaignPlural, campaignSingular, campaignType, compliancePlural, submissionPlural, submissionSingular, submissionType, timelinessPlural, timelinessSingular, timelinessType } from '#campaign/constants';
import { productPlural, productSingular, productType } from '#product/constants';
import { requirementPlural, requirementSingular, requirementType } from '#requirement/constants';
import { systemPlural, systemSingular, systemType } from '#settings/system';
import { useT } from 'apprise-frontend-core/intl/language';
import { Multilang } from 'apprise-frontend-core/intl/multilang';
import { TagCategory, useCategoryModel } from 'apprise-frontend-tags/category/model';
import { TagIcon } from 'apprise-frontend-tags/constants';
import { CustomProperty } from 'apprise-frontend-tags/customprop/model';
import { TaggedModule } from 'apprise-frontend-tags/tag/modules';
import { MultiBox, MultiboxProps } from 'apprise-ui/multibox/multibox';
import { SelectBox, SingleSelectProps } from 'apprise-ui/selectbox/selectbox';
import { SwitchBox } from 'apprise-ui/switchbox/switchbox';
import { allTypePropValue, capCategoryId, capDefaultProp, capTypeProp, complianceSingular, complianceType, complianceValueProp, timelinessValueProp } from './constants';
import { NumberBox } from 'apprise-ui/numberbox/numberbox';

export const systemTagModule: TaggedModule = ({

    type: systemType,
    singular: systemSingular,
    plural: systemPlural

})

export const campaignTagModule: TaggedModule = ({

    type: campaignType,
    singular: campaignSingular,
    plural: campaignPlural

})

export const requirementTagModule: TaggedModule = ({

    type: requirementType,
    singular: requirementSingular,
    plural: requirementPlural

})

export const productTagModule: TaggedModule = ({

    type: productType,
    singular: productSingular,
    plural: productPlural

})

export const complianceTagModule: TaggedModule = ({

    type: complianceType,
    singular: complianceSingular,
    plural: compliancePlural,
    customProperties: [complianceValueProp]

})

export const timelinessTagModule: TaggedModule = ({

    type: timelinessType,
    singular: timelinessSingular,
    plural: timelinessPlural,
    customProperties: [timelinessValueProp]

})

export const submissionTagModule: TaggedModule = ({

    type: submissionType,
    singular: submissionSingular,
    plural: submissionPlural

})


export const assetTagModule: TaggedModule = ({

    type: 'asset',
    singular: 'asset.singular',
    plural: 'asset.plural'

})

export const otherTagModule: TaggedModule = ({

    type: 'other',
    singular: 'other.singular',
    plural: 'other.plural'

})



export const useCapCategory = () : TagCategory => {

    const model  = useCategoryModel()

    return {
        ...model.newCategory(assetTagModule.type), 
        id: capCategoryId,
        lifecycle: { state: 'active' as const },
        name: {en:"Custom Asset Properties", fr:"Custom Asset Properties"},
        properties: {
            field: { enabled: false },
            custom : [capTypeProp,capDefaultProp ]
        }
    
    }
}

export const useCustomPropertiesTypes = () => {

    const t = useT()


    const capType : CustomProperty<string, SingleSelectProps<string>> = {

        id: capTypeProp,
        name: 'tag.cap_type_lbl',
        Icon: TagIcon,
        prototype: <SelectBox defaultValue={allTypePropValue} noClear options={[requirementType, productType,allTypePropValue]} render={s => t(`tag.cap_type_${s}`)} 
        
            label={t('tag.cap_type_lbl')} msg={t('tag.cap_type_msg')} help={t('tag.cap_type_help')} />,
        ValueBox: SelectBox

    }

    const capDefault : CustomProperty<Multilang, MultiboxProps> = {


        id: capDefaultProp,
        name: 'tag.cap_default_lbl',
        Icon: TagIcon,
        prototype: <SwitchBox label={t('tag.cap_default_lbl')} msg={t('tag.cap_default_msg')} help={t('tag.cap_default_help')} />,
        ValueBox: MultiBox

    }

    const complianceValue: CustomProperty<number, SingleSelectProps<number>> = {
        id: complianceValueProp,
        name: 'tag.compliance_value',
        Icon: TagIcon,
        prototype: <NumberBox label={t('tag.compliance_value_lbl')} msg={t('tag.compliance_value_msg')} help={t('tag.compliance_value_help')} />,
        ValueBox: NumberBox
    }

    const timelinessValue: CustomProperty<number, SingleSelectProps<number>> = {
        id: timelinessValueProp,
        name: 'tag.timeliness_value',
        Icon: TagIcon,
        prototype: <NumberBox label={t('tag.timeliness_value_lbl')} msg={t('tag.timeliness_value_msg')} help={t('tag.timeliness_value_help')} />,
        ValueBox: NumberBox
    }

    return [capType, capDefault, complianceValue, timelinessValue]
}