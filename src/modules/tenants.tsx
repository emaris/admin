import { approveCycleOptions } from "#campaign/model"
import { useT } from "apprise-frontend-core/intl/language"
import { utils } from "apprise-frontend-core/utils/common"
import { FormState } from "apprise-frontend-core/utils/form"
import { Tenant } from "apprise-frontend-iam/tenant/model"
import { TenantPreferencesModule } from "apprise-frontend-iam/tenant/modules"
import { MailTopicBox } from "apprise-frontend-mail/mailtopicbox"
import { MailTopic } from "apprise-frontend-mail/model"
import { ChoiceBox } from "apprise-ui/choicebox/choicebox"
import { useValidation } from "apprise-ui/field/validation"
import { Label } from "apprise-ui/label/label"
import { SelectBox } from "apprise-ui/selectbox/selectbox"
import { SwitchBox } from 'apprise-ui/switchbox/switchbox'
import { TextBox } from "apprise-ui/textbox/textbox"
import { TimezoneModule, useTimezonesRegistry } from "apprise-ui/timezone/api"
import { ZonePickerBox } from "apprise-ui/timezone/zonepicker"
import { Fragment } from "react"

export type EmarisTenantMailProfile = {

    // these are set in the user form.
    mailProfile: {

        topics: MailTopic[]
    }
}

export type EmarisTenantPreferences = EmarisTenantMailProfile & {


    bypassSelfApproval: boolean
}

const defaultPreferences: EmarisTenantMailProfile = {

    mailProfile: {

        topics: []
    }
}

const useFields = (props: FormState<Tenant>) => {

    const t = useT()
    const { check, is } = useValidation()

    const { edited } = props

    return {

        'timezone': {
            label: t('campaign.location_lbl'),
            msg: t('campaign.location_msg'),
            help: t("campaign.location_help")
        }

        ,

        'preferredTimezone': {
            label: t('campaign.default_location_lbl'),
            msg: t('campaign.default_location_msg'),
            help: <>
                {t("campaign.default_location_help1")}<br />
                {t("campaign.default_location_help2")}
            </>
        }

        ,

        'approveCycle': {
            label: t('campaign.approve_cycle_lbl'),
            msg: t('campaign.approve_cycle_msg'),
            help: <>
                {t("campaign.approve_cycle_help1")}<br />
                {t("campaign.approve_cycle_help2")}
            </>
        }

        ,

        'bypassSelfApproval': {

            label: t("campaign.bypass_approval_lbl"),
            msg: t("campaign.bypass_approval_msg"),
            help: t("campaign.bypass_approval_help")
        }
        ,

        'email': {
            label: t('tenant.email_lbl'),
            msg: t('tenant.email_msg'),
            help: t("tenant.email_help"),

            ... (edited.preferences?.email && edited.preferences.email !== '') && check(is.empty).and(is.badEmail).on(edited.preferences.email)
        }
    }
}

const Form = (props: FormState<Tenant>) => {

    const { edited, set } = props

    const t = useT()

    const timezoneRegistry = useTimezonesRegistry()

    const report = useFields(props)

    const prefs = utils().merge(defaultPreferences,edited.preferences) as EmarisTenantPreferences

    return <Fragment>

        <ZonePickerBox 
            info={report.timezone} 
            includeLocal={false} 
            onChange={set.with((u, v) => u.preferences.location = v.option)} 
        >
            {edited.preferences.location}
        </ZonePickerBox>

        <SelectBox<TimezoneModule> 
            keyOf={tz => tz.name}
            label={report.preferredTimezone.label} 
            info={report.preferredTimezone}
            options={timezoneRegistry.all()}
            onChange={set.with((u, v) => u.preferences.dateLocation = v.name)}
            render={pt => <Label noIcon title={t('time.preferred_timezone_name', {value: pt.label})} />}>
            {timezoneRegistry.all().find( tz => tz.name === edited.preferences.dateLocation)}
        </SelectBox>

        <ChoiceBox<string> radio
            info={report.approveCycle}
            value={edited.preferences.approveCycle}
            onChange={set.with((u, v) => u.preferences.approveCycle = v)}>
            {Object.keys(approveCycleOptions).map((ac, idx) => <ChoiceBox.Option key={idx} title={t(approveCycleOptions[ac])} value={ac} />)}
        </ChoiceBox>

        <SwitchBox info={report.bypassSelfApproval} onChange={set.with((u, v) => u.preferences.bypassSelfApproval = v)}>
            {edited.preferences.bypassSelfApproval}
        </SwitchBox>

        <MailTopicBox onChange={set.with((m, v) => {

            const profile: EmarisTenantMailProfile = { mailProfile: { ...prefs.mailProfile, topics: v } }

            m.preferences = { ...prefs, ...profile }

        })}>
            {prefs.mailProfile?.topics}
        </MailTopicBox>

        <TextBox info={report.email} onChange={set.with((u, v) => u.preferences.email = v)}>{edited.preferences.email}</TextBox>

    </Fragment>

}

export const tenantPreferencesModule: TenantPreferencesModule = {

    fields: useFields,
    Form
}