
import { AdminConsoleConfig } from '#config'
import { systemSettingsModule } from '#modules/settings'
import { useConfig } from 'apprise-frontend-core/config/api'
import { useSettings } from 'apprise-frontend-streams/settings/api'

export type DateFormatOption = {
    name: string
    format: string
}

export type ImageSizeOption = {
    name: string
    val: number
}

export type AdminConsoleSettings = {
    crosscontext : boolean
    defaultContext : string
    defaultProductImportance : string[]
    defaultRequirementImportance : string[]
}

export type AppSettings = AdminConsoleSettings & {
    dateFormat: string
}



export const useAppSettings = () => {

    const { dateFormats } = useConfig<AdminConsoleConfig>()

    const systemsettings = useSettings<AppSettings>(systemSettingsModule.type)
    
    const self = {

        ...systemsettings

        ,

        resolveFormat: () => dateFormats[systemsettings.dateFormat]?.format,

    }


    return self

}

