

import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'
import { useT } from 'apprise-frontend-core/intl/language'
import { SettingRenderProps } from 'apprise-frontend-streams/settings/model'
import { contextCategory } from 'apprise-frontend-tags/constants'
import { TagRefBox } from 'apprise-frontend-tags/tag/tagrefbox'
import { Form } from 'apprise-ui/form/form'
import { SwitchBox } from 'apprise-ui/switchbox/switchbox'
import { AppSettings } from './settings'

export const systemType = "system"
export const systemSingular = "system.singular"
export const systemPlural = "system.plural"


export const SystemSettingsPanel = (props: SettingRenderProps<AppSettings>) => {

    const { settings, onChange } = props

    const t = useT()
    
    return <Form>
        <SwitchBox 
            label={t('appsettings.crossctx_name')} 
            msg={t('appsettings.crossctx_msg')} 
            help={t('appsettings.crossctx_help')}
            onChange={v => onChange({ ...settings, crosscontext: !!v })}>
            {settings.crosscontext}
        </SwitchBox>

        <TagRefBox 
            noClear
            type={systemType}
            category={contextCategory}
            onChange={t => onChange({...settings, defaultContext: t!})}>
            {settings.defaultContext}
        </TagRefBox>

        <TagRefBox
            noClear
            type={requirementType} 
            category={'TGC-requirement-importancescale'}
            onChange={t => onChange({...settings, defaultRequirementImportance: [t!]})}>
            {settings.defaultRequirementImportance?.[0]}
        </TagRefBox>

        <TagRefBox
            noClear
            type={productType} 
            category={'TGC-product-importancescale'}
            onChange={t => onChange({...settings, defaultProductImportance: [t!]})}>
            {settings.defaultProductImportance?.[0]}
        </TagRefBox>


    </Form>
}



