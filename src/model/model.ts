import { Multilang } from "apprise-frontend-core/intl/multilang"
import { Lifecycled } from "apprise-frontend-core/utils/lifecycle"
import { Tagged } from "apprise-frontend-tags/tag/model"

export type AssetState = "active" | "inactive"

export type AudienceInclusions = 'includes' | 'excludes'

export type TenantAudience = {
    [key in AudienceInclusions]: string[]
}

export type NamedAsset = {

    id: string
    asset: string
    alias: string
    type: 'product' | 'requirement'

}


export type AssetType = Tagged & Lifecycled<AssetState> & {

    id: string

    predefined?: boolean

    name: Multilang
    description: Multilang

    audienceList?: TenantAudience

}
